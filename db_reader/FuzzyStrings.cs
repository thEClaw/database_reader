﻿using System;
using System.Linq;
using System.Text.RegularExpressions;


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* Copied from https://github.com/tylerjensen/duovia-fuzzystrings/tree/master/src/DuoVia.FuzzyStrings/DuoVia.FuzzyStrings
* All (or most) credit to https://github.com/tylerjensen
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

namespace FuzzyStrings
{
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 	* Derived from http://www.codeguru.com/vb/gen/vb_misc/algorithms/article.php/c13137__1/Fuzzy-Matching-Demo-in-Access.htm
 	* and from http://www.berghel.net/publications/asm/asm.php 
 	* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
	public static class LevenshteinDistanceExtensions
	{
		// normalize the output to [0, 1] with 1 being the best possible result
		public static double Normalized(this string input, string comparedTo)
		{
			return 1.0 - (double)LevenshteinDistance (input, comparedTo) / Math.Max (input.Length, comparedTo.Length);
//			return 1.0 - LevenshteinDistanceNormalized (input, comparedTo); // output is extremely close to the one by the method above
		}

		public static double LevenshteinDistanceNormalized(this string input, string comparedTo, bool caseSensitive = false, bool primitiveMethod = false)
		{
			if (!caseSensitive)
			{
				input = input.ToLower();
				comparedTo = comparedTo.ToLower();
			}
			if (string.Compare(input, comparedTo, StringComparison.CurrentCulture) == 0) {
				return 0.0;
			}
			int inputLen = input.Length;
			int comparedToLen = comparedTo.Length;

			if (inputLen == 0 || comparedToLen == 0)
				return 1.0;
			
			if (inputLen < comparedToLen) { // minimize the arrays size
				int buffer = inputLen;
				inputLen = comparedToLen;
				comparedToLen = buffer;

				string sBuffer = input;
				input = comparedTo;
				comparedTo = sBuffer;
			}

			if (primitiveMethod)
				return LevenshteinDistance(input, comparedTo) / (double)inputLen;

			int[] column = new int[comparedToLen + 1]; // array is 1 bigger than the string
			int[] length = new int[comparedToLen + 1];

			for (int i = 0; i < column.Length; i++) {
				column [i] = i;
				length [i] = i;
			}

			for (int x = 1; x < inputLen + 1; x++) {
				column [0] = length [0] = x;
				int last = x - 1, llast = x - 1;

				for (int y = 1; y < comparedToLen + 1; y++) {
					// dist
					int old  = column [y];
					int ic   = column [y - 1] + 1;
					int dc   = column [y] + 1;
					int rc   = last + ((input [x - 1] != comparedTo [y - 1]) ? 1 : 0);
					column [y] = FindMinimum (new int[]{ ic, dc, rc });
					last     = old;

					// length
					int lold = length[y];
					int lic  = (ic == column [y]) ? length [y - 1] + 1 : 0;
					int ldc  = (dc == column [y]) ? length [y] + 1 : 0;
					int lrc  = (rc == column [y]) ? llast + 1 : 0;
					length [y] = FindMaximum (new int[]{ ldc, lic, lrc});
					llast    = lold;
				}
			}

			return (float)column [comparedToLen] / length [comparedToLen];
		}

		/// <summary>
		/// Levenshtein Distance algorithm with transposition. <br />
		/// A value of 1 or 2 is okay, 3 is iffy and greater than 4 is a poor match
		/// </summary>
		/// <param name="input"></param>
		/// <param name="comparedTo"></param>
		/// <param name="caseSensitive"></param>
		/// <returns></returns>
		public static int LevenshteinDistance(this string input, string comparedTo, bool caseSensitive = false)
		{
			//if (string.IsNullOrWhiteSpace(input) || string.IsNullOrWhiteSpace(comparedTo)) return -1;
			if (!caseSensitive)
			{
				input = input.ToLower();
				comparedTo = comparedTo.ToLower();
			}
			if (string.Compare(input, comparedTo, StringComparison.CurrentCulture) == 0) {
				return 0;
			}
			int inputLen = input.Length;
			int comparedToLen = comparedTo.Length;

			if (inputLen == 0)
				return comparedToLen;
			if (comparedToLen == 0)
				return inputLen;

			int[,] matrix = new int[inputLen, comparedToLen];

			//initialize
			for (int i = 0; i < inputLen; i++) matrix[i, 0] = i;
			for (int i = 0; i < comparedToLen; i++) matrix[0, i] = i;

			//analyze
			for (int i = 1; i < inputLen; i++)
			{
				var si = input[i - 1];
				for (int j = 1; j < comparedToLen; j++)
				{
					var tj = comparedTo[j - 1];
					int cost = (si == tj) ? 0 : 1;

					var above = matrix[i - 1, j];
					var left = matrix[i, j - 1];
					var diag = matrix[i - 1, j - 1];
					var cell = FindMinimum(above + 1, left + 1, diag + cost);

					//transposition
					if (i > 1 && j > 1)
					{
						var trans = matrix[i - 2, j - 2] + 1;
						if (input[i - 2] != comparedTo[j - 1]) trans++;
						if (input[i - 1] != comparedTo[j - 2]) trans++;
						if (cell > trans) cell = trans;
					}
					matrix[i, j] = cell;
				}
			}
			return matrix[inputLen - 1, comparedToLen - 1];
		}

		private static int FindMinimum(params int[] p)
		{
			if (null == p) return int.MinValue;
			int min = int.MaxValue;
			for (int i = 0; i < p.Length; i++)
			{
				if (min > p[i]) min = p[i];
			}
			return min;
		}

		private static int FindMaximum(params int[] p)
		{
			if (null == p) return int.MaxValue;
			int max = int.MinValue;
			for (int i = 0; i < p.Length; i++)
			{
				if (max < p[i]) max = p[i];
			}
			return max;
		}
	}


	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 	* Derived from http://www.codeproject.com/KB/recipes/lcs.aspx 
 	* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
	public static class LongestCommonSubsequenceExtensions
	{
		/// <summary>
		/// Longest Common Subsequence. A good value is greater than 0.33.
		/// </summary>
		/// <param name="input"></param>
		/// <param name="comparedTo"></param>
		/// <param name="caseSensitive"></param>
		/// <returns>Returns a Tuple of the sub sequence string and the match coeficient.</returns>
		public static Tuple<string, double> LongestCommonSubsequence(this string input, string comparedTo, bool caseSensitive = false)
		{
			if (string.IsNullOrWhiteSpace(input) || string.IsNullOrWhiteSpace(comparedTo))
				return new Tuple<string, double>(string.Empty, 0.0d);
			if (!caseSensitive)
			{
				input = input.ToLower();
				comparedTo = comparedTo.ToLower();
			}

			int inputLen = input.Length;
			int comparedToLen = comparedTo.Length;

			int[,] lcs = new int[inputLen + 1, comparedToLen + 1];
			LcsDirection[,] tracks = new LcsDirection[inputLen + 1, comparedToLen + 1];
			int[,] w = new int[inputLen + 1, comparedToLen + 1];
			int i, j;

			for (i = 0; i <= inputLen; ++i)
			{
				lcs[i, 0] = 0;
				tracks[i, 0] = LcsDirection.North;

			}
			for (j = 0; j <= comparedToLen; ++j)
			{
				lcs[0, j] = 0;
				tracks[0, j] = LcsDirection.West;
			}

			for (i = 1; i <= inputLen; ++i)
			{
				for (j = 1; j <= comparedToLen; ++j)
				{
					if (input[i - 1].Equals(comparedTo[j - 1]))
					{
						int k = w[i - 1, j - 1];
						//lcs[i,j] = lcs[i-1,j-1] + 1;
						lcs[i, j] = lcs[i - 1, j - 1] + Square(k + 1) - Square(k);
						tracks[i, j] = LcsDirection.NorthWest;
						w[i, j] = k + 1;
					}
					else
					{
						lcs[i, j] = lcs[i - 1, j - 1];
						tracks[i, j] = LcsDirection.None;
					}

					if (lcs[i - 1, j] >= lcs[i, j])
					{
						lcs[i, j] = lcs[i - 1, j];
						tracks[i, j] = LcsDirection.North;
						w[i, j] = 0;
					}

					if (lcs[i, j - 1] >= lcs[i, j])
					{
						lcs[i, j] = lcs[i, j - 1];
						tracks[i, j] = LcsDirection.West;
						w[i, j] = 0;
					}
				}
			}

			i = inputLen;
			j = comparedToLen;

			string subseq = "";
			double p = lcs[i, j];

			//trace the backtracking matrix.
			while (i > 0 || j > 0)
			{
				switch (tracks[i, j])
				{
					case LcsDirection.NorthWest:
						i--;
						j--;
						subseq = input[i] + subseq;
						//Trace.WriteLine(i + " " + input1[i] + " " + j);
						break;
					case LcsDirection.North:
						i--;
						break;
					case LcsDirection.West:
						j--;
						break;
				}
			}

			double coef = p / (inputLen * comparedToLen);

			var retval = new Tuple<string, double>(subseq, coef);
			return retval;
		}

		private static int Square(int p)
		{
			return p * p;
		}
	}

	internal enum LcsDirection
	{
		None,
		North,
		West,
		NorthWest
	}


	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 	* Derived from http://www.codeguru.com/vb/gen/vb_misc/algorithms/article.php/c13137__1/Fuzzy-Matching-Demo-in-Access.htm
 	* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
	public static class DiceCoefficientExtensions
	{
		/// <summary>
		/// Dice Coefficient based on bigrams. <br />
		/// A good value would be 0.33 or above, a value under 0.2 is not a good match, from 0.2 to 0.33 is iffy.
		/// </summary>
		/// <param name="input"></param>
		/// <param name="comparedTo"></param>
		/// <returns></returns>
		public static double DiceCoefficient(this string input, string comparedTo)
		{
			var ngrams = input.ToBiGrams();
			var compareToNgrams = comparedTo.ToBiGrams();
			return ngrams.DiceCoefficient(compareToNgrams);
		}

		/// <summary>
		/// Dice Coefficient used to compare nGrams arrays produced in advance.
		/// </summary>
		/// <param name="nGrams"></param>
		/// <param name="compareToNGrams"></param>
		/// <returns></returns>
		public static double DiceCoefficient(this string[] nGrams, string[] compareToNGrams)
		{
			int matches = 0;
			foreach (var nGram in nGrams)
			{
				if (compareToNGrams.Any(x => x == nGram)) matches++;
			}
			if (matches == 0) return 0.0d;
			double totalBigrams = nGrams.Length + compareToNGrams.Length;
			return (2 * matches) / totalBigrams;
		}

		public static string[] ToBiGrams(this string input)
		{
			// nLength == 2
			//   from Jackson, return %j ja ac ck ks so on n#
			//   from Main, return #m ma ai in n#
			input = SinglePercent + input + SinglePound;
			return ToNGrams(input, 2);
		}

		public static string[] ToTriGrams(this string input)
		{
			// nLength == 3
			//   from Jackson, return %%j %ja jac ack cks kso son on# n##
			//   from Main, return ##m #ma mai ain in# n##
			input = DoublePercent + input + DoublePount;
			return ToNGrams(input, 3);
		}

		private static string[] ToNGrams(string input, int nLength)
		{
			int itemsCount = input.Length - 1;
			string[] ngrams = new string[input.Length - 1];
			for (int i = 0; i < itemsCount; i++) ngrams[i] = input.Substring(i, nLength);
			return ngrams;
		}

		private const string SinglePercent = "%";
		private const string SinglePound = "#";
		private const string DoublePercent = "&&";
		private const string DoublePount = "##";
	}


	public static class StringExtensions  // requires DoubleMetaphoneExtension to work
	{
		public static bool FuzzyEquals(this string strA, string strB, double requiredProbabilityScore = 0.75)
		{
			return strA.FuzzyMatch(strB) > requiredProbabilityScore;
		}

		public static double FuzzyMatch(this string strA, string strB)
		{
			string localA = Strip(strA.Trim().ToLower());
			string localB = Strip(strB.Trim().ToLower());
			if (localA.Contains(space) && localB.Contains(space))
			{
				var partsA = localA.Split(' ');
				var partsB = localB.Split(' ');
				var weightedHighCoefficients = new double[partsA.Length];
				//var distanceRatios = new double[partsA.Length];
				for (int i = 0; i < partsA.Length; i++)
				{
					double high = 0.0;
					int indexDistance = 0;
					for (int x = 0; x < partsB.Length; x++)
					{
						var coef = CompositeCoefficient(partsA[i], partsB[x]);
						if (coef > high)
						{
							high = coef;
							indexDistance = Math.Abs(i - x);
						}
					}
					double distanceWeight = indexDistance == 0 ? 1.0 : 1.0 - (indexDistance / (partsA.Length * 1.0));
					weightedHighCoefficients[i] = high * distanceWeight;
				}
				double avgWeightedHighCoefficient = weightedHighCoefficients.Sum() / (partsA.Length * 1.0);
				return avgWeightedHighCoefficient < 0.999999 ? avgWeightedHighCoefficient : 0.999999; //fudge factor
			}
			else
			{
				var singleComposite = CompositeCoefficient(localA, localB);
				return singleComposite < 0.999999 ? singleComposite : 0.999999; //fudge factor
			}
		}

		private static readonly string space = " ";
		private static Regex stripRegex = new Regex(@"[^a-zA-Z0-9 -]*");

		private static string Strip(string str)
		{
			return stripRegex.Replace(str, string.Empty);
		}

		private static double CompositeCoefficient(string strA, string strB)
		{
			double dice = strA.DiceCoefficient(strB);
			var lcs = strA.LongestCommonSubsequence(strB);
			int leven = strA.LevenshteinDistance(strB);
			double levenCoefficient = 1.0 / (1.0 * (leven + 0.2)); //may want to tweak offset
			string strAMp = strA.ToDoubleMetaphone();
			string strBMp = strB.ToDoubleMetaphone();
			int matchCount = 0;
			if (strAMp.Length == 4 && strBMp.Length == 4)
			{
				for (int i = 0; i < strAMp.Length; i++)
				{
					if (strAMp[i] == strBMp[i]) matchCount++;
				}
			}
			double mpCoefficient = matchCount == 0 ? 0.0 : matchCount / 4.0;
			double avgCoefficent = (dice + lcs.Item2 + levenCoefficient + mpCoefficient) / 4.0;
			return avgCoefficent;
		}
	}
}
