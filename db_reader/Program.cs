﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;

using FuzzyStrings;

using Mono.Data.Sqlite;
using System.Windows.Forms;

namespace db_reader
{
	class Constants
	{
		public const string base_dir = "mobygames_crawler";
		public const string db_suffix = ".db";
		public const string db_folder = "data";
		public static string arg_xml = "settings.xml";
	}

	[SqliteFunction(Name = "FUZZY", Arguments = 2, FuncType = FunctionType.Scalar)]
	class SQLFilter : SqliteFunction
	{
		public override object Invoke(object[] args)
		{
			string input = args[0].ToString(), comparedTo = args[1].ToString();

			//add column in SELECT: ({1}) AS __quality

			double qRegex, qLevenshtein, qDice, qSubsequence, qMetaphone;
			qRegex = qLevenshtein = qDice = qSubsequence = qMetaphone = 0.0;

			bool result = true;
			if ((f & Filter.regex) != 0                   || discriminator == Filter.regex) {
				qRegex = (Regex.IsMatch (input, ".*" + Regex.Escape (comparedTo) + ".*") ? 1.0 : 0.0);
				result = result && (qRegex == 1.0);
			}
			if ((result && (f & Filter.levenshtein) != 0) || discriminator == Filter.levenshtein) {
				qLevenshtein = input.Normalized (comparedTo);
				result = result && qLevenshtein >= 0.40;
			}
			if ((result && (f & Filter.dice) != 0)        || discriminator == Filter.dice) {
				qDice = input.DiceCoefficient (comparedTo);
				result = result && qDice >= 0.35;
			}
			if ((result && (f & Filter.subsequence) != 0) || discriminator == Filter.subsequence) {
				qSubsequence = input.LongestCommonSubsequence (comparedTo).Item2;
				result = result && qSubsequence >= 0.30;
			}
			if ((result && (f & Filter.metaphone) != 0)   || discriminator == Filter.metaphone) {
				qMetaphone = input.FuzzyMatch (comparedTo);
				result = result && qMetaphone > 0.50;  // quite slow!
			}

			switch (discriminator) {
			case Filter.regex:
				return (result ? qRegex : 0.0);
			case Filter.levenshtein:
				return (result ? qLevenshtein : 0.0);
			case Filter.dice:
				return (result ? qDice : 0.0);
			case Filter.subsequence:
				return (result ? qSubsequence : 0.0);
			case Filter.metaphone:
				return (result ? qMetaphone : 0.0);
			default:
				return 0.0;
			}
		}

		[Flags]
		private enum Filter
		{
			none = 0x0,
			regex = 0x1,
			levenshtein = 0x2,
			dice = 0x4,
			subsequence = 0x8,
			metaphone = 0x10,
			allfast = levenshtein | dice | subsequence,
			allfuzzy = levenshtein | dice | subsequence | metaphone,
			all = levenshtein | dice | subsequence | metaphone | regex
		};
		private Filter f = Filter.allfast;
		private Filter discriminator = Filter.subsequence;  // should never be a combination (or the function always returns 0.0)
	}

	class MainClass
	{
		[STAThread]
		public static int Main()
		{
			Constants.arg_xml = Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + Constants.arg_xml; // fix this path for later

            if (Environment.CurrentDirectory.Contains(Constants.base_dir))  // change directory to base_dir
			{
				while (!Environment.CurrentDirectory.EndsWith(Constants.base_dir, StringComparison.Ordinal))
					Environment.CurrentDirectory = Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + "..";
			}
			else if (!Directory.Exists(Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + Constants.db_folder))
			{
				Console.WriteLine(string.Format("Execute this program from within the \"{0}\"-directory or move the \'{1}\'-folder into the same directory as this executable.",
				                                Constants.base_dir, Constants.db_folder));
				return 1;
			}

			var cmdLine = new CommandLineArguments();
			cmdLine.RestoreFromXml(Constants.arg_xml);
			cmdLine.Initialize();
			// Validate only after checking to see if they requested help in order to prevent displaying errors when the user requested help.
			if (cmdLine.Help || !cmdLine.IsValid())
			{
				Console.WriteLine(cmdLine.GetHelpText(Console.WindowWidth));
				return 0;
			}
			cmdLine.SaveToXml(Constants.arg_xml);

			Console.WriteLine();
			Console.WriteLine("--------------------SETTINGS-------------------");
			Console.WriteLine("Country:        " + cmdLine.Country);
			Console.WriteLine("Databases:      " + string.Join(", ", cmdLine.Platforms));
			Console.WriteLine("Autocopy:       " + cmdLine.AutoCopy);
			Console.WriteLine("Output:         " + cmdLine.OutputFormat);
			Console.WriteLine("Order by:       " + (cmdLine.Order ? "output-strings" : "quality"));
			Console.WriteLine("Max. Results:   " + cmdLine.Results);
			Console.WriteLine("Search through: " + cmdLine.Search);
			Console.WriteLine("Search for:     " + cmdLine.Keyword);
			Console.WriteLine();

			var outputList = new List<KeyValuePair<double, string>>();
			var re = new Regex("%(\\w+)%");  // for replacing strings in desired OutputFormat
			foreach (string db in cmdLine.Platforms)
			{
				/* Copied from Python:
				platform = re.sub("[^\w]", "_", platform).strip().lower()
			    platform = re.sub("_{2,}", "_", platform)  # avoid multiple underscores next to each other
			    tablen = country_codes[country].lower() + "_" + platform
				 */
				string platform = db.Remove(db.Length - Constants.db_suffix.Length).Remove(0, "mobygames_".Length);
				string tablen = CountryCodes.by_country[cmdLine.Country].ToLower() + "_" + platform;
				string connectionString = string.Format("Data Source={0},Version=3,",
					Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + Constants.db_folder + Path.DirectorySeparatorChar + cmdLine.Country + Path.DirectorySeparatorChar + db);

				IDbConnection connection = new SqliteConnection(connectionString);
				connection.Open();

				IDbCommand command = connection.CreateCommand();
				command.CommandText = Query.Build(tablen, cmdLine.Search, cmdLine.Keyword, cmdLine.Results);

				IDataReader reader = command.ExecuteReader();
				while (reader.Read())
				{
					// iterates over matching rows
					string output = cmdLine.OutputFormat;

					Match match = re.Match(output);  // picks the first match
					while (match.Success)
					{
						string write = reader.GetValue(reader.GetOrdinal(match.Groups[1].Captures[0].ToString())).ToString();  // group-indices start at 1
						output = re.Replace(output, write, 1);

						match = match.NextMatch();  // iterates to the next match
					}

					outputList.Add(new KeyValuePair<double, string>(reader.GetDouble(reader.GetOrdinal("__quality")), output));
				}

				// clean up
				reader.Dispose();
				command.Dispose();
				connection.Close();
			}

			Console.WriteLine("\n--------------------RESULTS--------------------");
			if (outputList.Count == 0)
				Console.WriteLine("                 === NONE ===");

			//sort and shorten the final list (necessary in case several platforms are looked at since SQL returns a *full* list for each platform)
			if (cmdLine.Order) {
				outputList.Sort ((x, y) => x.Value.CompareTo (y.Value)); // order alphabetically, using the output-pattern
			} else {
				outputList.Sort((x, y) => x.Key.CompareTo(y.Key));
				outputList.Reverse();
			}

			outputList = outputList.GetRange(0, Math.Min(outputList.Count, cmdLine.Results));
			foreach (KeyValuePair<double, string> pair in outputList)
			{
				//Console.WriteLine (String.Format ("{0} - {1}", pair.Key, pair.Value));
				Console.WriteLine(pair.Value);
			}

			if (cmdLine.AutoCopy == true && outputList.Count > 0)
			{
				Clipboard.SetText(outputList[0].Value);  // TODO: does not work under Linux (as did none of the alternatives); DOES work under Windows, though
			}

			return 0;
		}
	}
}
