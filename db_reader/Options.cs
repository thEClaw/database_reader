﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

using BizArk.Core;
using BizArk.Core.CmdLine;

namespace db_reader
{
	[CmdLineOptions(ArgumentPrefix = "-", Description = "Looks up a keyword in one of the (offline) mobygames-databases and returns a formatted string containing information.")]
	class CommandLineArguments : CmdLineObject
	{
        public CommandLineArguments()  // defaults before C#6.0 had to be set manually; synatx 'public string Country { get; set; } = "Germany";' was invalid
        {
            Country = "Germany";
            Search = "title";
			Order = false;
            Results = 15;
            AutoCopy = false;
            OutputFormat = "%title% (%developer%)(%publisher%)(%year%)";
            Platforms = new string[] { "Windows", "DOS" };
        }

		[CmdLineArg(Aliases = new string[] { "c", "-country" }, ShowInUsage = DefaultBoolean.True, AllowSave = true)]
		[System.ComponentModel.Description("Country or country-code for which data will be loaded.")]
		public string Country { get; set; }

		[CmdLineArg(Aliases = new string[] { "s", "-search" }, ShowInUsage = DefaultBoolean.True, AllowSave = true)]
		[System.ComponentModel.Description("What to search for - common values: title, publisher, developer, genre, setting or year.")]
		public string Search { get; set; }

		[CmdLineArg(Aliases = new string[] { "z", "-order_alphabetically" }, ShowInUsage = DefaultBoolean.True, AllowSave = false)]
		[System.ComponentModel.Description("Order according to the output-pattern instead of by quality of the results.")]
		public bool Order { get; set; }

		[CmdLineArg(Aliases = new string[] { "n", "-results" }, ShowInUsage = DefaultBoolean.True, AllowSave = true)]
		[System.ComponentModel.Description("Maximum amount of results shown at the end.")]
		public int Results { get; set; }

		[CmdLineArg(Aliases = new string[] { "a", "-autocopy" }, ShowInUsage = DefaultBoolean.True, AllowSave = true)]
		[System.ComponentModel.Description("Automatically copy best result to the clipboard. (not yet working)")]
		public bool AutoCopy { get; set; }

		[CmdLineArg(Aliases = new string[] { "o", "-output" }, ShowInUsage = DefaultBoolean.True, AllowSave = true)]
		[System.ComponentModel.Description("Format of the output, each keyword has to be enclosed in '%'. Possible values: title, link, description, developer, publisher, released, year, cscore, uscore, educational, gameplay, genre, perspective, setting, vehicular")]
		public string OutputFormat { get; set; }

		[CmdLineArg(Aliases = new string[] { "p", "-platforms" }, ShowInUsage = DefaultBoolean.True, AllowSave = true)]
		[System.ComponentModel.Description("Platforms for which data shall be searched, separated by a semicolon. \"all\" to use all available platforms.")]
		public string[] Platforms { get; set; }

		[CmdLineArg(Aliases = new string[] { "k", "-key" }, ShowInUsage = DefaultBoolean.True, Required = true, AllowSave = false)]
		[System.ComponentModel.Description("Keyword to search for, usually a games name.")]
		public string Keyword { get; set; }

		public Dictionary<string, string> Keywords {
			get {
				string[] pairs = Keyword.Split (new Char[] {';'}, StringSplitOptions.RemoveEmptyEntries);  // should always have at least 1 element
				Dictionary<string, string> dout = new Dictionary<string, string>();
				foreach (string p in pairs) {
					string[] kv = p.Split (new Char[] {'='}, 2, StringSplitOptions.None);
					if (kv.Length != 2 && pairs.Length == 1)
						return new Dictionary<string, string>{ {Search, Keyword} };
					if (kv.Length != 2 || string.IsNullOrEmpty (kv [1]) || string.IsNullOrEmpty (kv [0])) {
						Console.WriteLine ("WARNING: Parameter skipped: {0}", p);
						continue;
					}
					dout.Add (kv [0], kv [1]);
				}
				return dout;
			}
		}

		// validation
		protected override string[] Validate()
		{
			if (string.IsNullOrEmpty(Country))
			{
				Country = "Germany";
			}
			else if (Country.Length == 2)
			{
				if (CountryCodes.by_code.ContainsKey(Country.ToUpper()))
				{
					Country = CountryCodes.by_code[Country.ToUpper()];
				}
				else
				{
					Country = "Germany";
				}
			}
			if (!Directory.Exists(Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + Constants.db_folder + Path.DirectorySeparatorChar + Country)
				|| Directory.GetFiles(Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + Constants.db_folder + Path.DirectorySeparatorChar + Country,
					"*" + Constants.db_suffix).Length < 1)
			{
				return new[] { string.Format("No database(s) found for: \"{0}\"", Country) };
			}

			if (string.IsNullOrEmpty(Search))
			{
				Search = "title";
			}

			if (Results < 1)
			{
				Results = 15;
			}

			if (string.IsNullOrEmpty(OutputFormat))
			{
				return new[] { string.Format("Formatted output will always be empty: \"{0}\"", OutputFormat) };
			}

			// check if the given database(s) exists (at least one of them must)
			if (Platforms.Length < 1 || string.Join("", Platforms).Length == 0)
			{
				return new[] { "No platform(s) given." };
			}
			else if (Platforms.Length == 1 && Platforms[0].Contains(";"))
			{
				Platforms = Platforms[0].Split(';');
			}

			if (Platforms.Length < 1 || string.Join ("", Platforms).Length == 0) {
				return new[] { "No platform(s) given." };
			} else if (Platforms[0] == "all") {
				var _databases = new List<string>();
				string[] files = Directory.GetFiles (Directory.GetCurrentDirectory () + Path.DirectorySeparatorChar + Constants.db_folder + Path.DirectorySeparatorChar + Country + Path.DirectorySeparatorChar, "mobygames_*" + Constants.db_suffix);
				foreach (string s in files) {
					string[] parts = s.Split (new char[]{ '/', '\\' });
					if (parts.Length == 0)
						continue;
					_databases.Add(parts [parts.Length - 1]);
				}
				if (_databases.Count < 1)
				{
					if (!Platforms[0].StartsWith("mobygames_", StringComparison.Ordinal) || !Platforms[0].EndsWith(Constants.db_suffix, StringComparison.Ordinal))
					{  // check if old settings had been loaded
						return new[] { "No databases found." };
					}
				}
				else
				{
					Platforms = _databases.ToArray();
				}
			}
			else
			{
				var _databases = new List<string>(25);
				foreach (string platform in Platforms)
				{
					/* Copied from Python:
					platform = re.sub("[^\w]", "_", platform).strip().lower()
					platform = re.sub("_{2,}", "_", platform)  # avoid multiple underscores next to each other
					"mobygames_" + platform + ".db"))*/
					string db = Regex.Replace(platform, "[^\\w]", "_").Trim().ToLower();
					db = Regex.Replace(db, "_{2,}", "_");  // avoid multiple underscores next to each other
					if (File.Exists(Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + Constants.db_folder + Path.DirectorySeparatorChar + Country + Path.DirectorySeparatorChar + "mobygames_" + db + Constants.db_suffix))
						_databases.Add("mobygames_" + db + Constants.db_suffix);
				}
				if (_databases.Count < 1)
				{
					if (!Platforms[0].StartsWith("mobygames_", StringComparison.Ordinal) || !Platforms[0].EndsWith(Constants.db_suffix, StringComparison.Ordinal))
					{  // check if old settings had been loaded
						return new[] { string.Format("No databases for the given platform(s) found: \"{0}\"", string.Join(", ", Platforms)) };
					}
				}
				else
				{
					Platforms = _databases.ToArray();
				}
			}

			return base.Validate();
		}
	}
}

