﻿using System;
using System.Collections.Generic;

namespace db_reader
{
	public static class Query
	{
		// group all possible data
		private static List<string> no_ref              = new List<string>{ "link", "description", "released", "year", "cscore", "uscore"};
		private static List<string> single_ref_direct   = new List<string>{ "publisher", "developer"};
		private static List<string> single_ref_indirect = new List<string>{ "title" };  // not "alt_titles", they are included here
		private static List<string> dual_ref_indirect   = new List<string>{ "educational", "gameplay", "genre", "perspective", "setting", "vehicular"};

		public static string Build (string tablen, string column, string key, int maxResults)
		{
			if (no_ref.Contains (column))
				return Build_no_ref (tablen, column, key, maxResults);
			if (single_ref_direct.Contains (column))
				return Build_single_ref_direct (tablen, column, key, maxResults);
			if (single_ref_indirect.Contains (column))
				return Build_single_ref_indirect (tablen, column, key, maxResults);
			if (dual_ref_indirect.Contains (column))
				return Build_dual_ref_indirect (tablen, column, key, maxResults);
			else
				throw new System.ArgumentException("Column does not exist: ", column);
		}

		private static Dictionary<string, string> aliases = new Dictionary<string, string> {
			{ "publisher", "pt.value" },
			{ "developer", "dt.value" },
		};

		private static string _build_base (string tablen, string extraColumn = "")
		{
			return string.Format(@"
				SELECT m.title AS title, m.link AS link, m.description AS description, m.released AS released, m.year AS year,
					       m.cscore AS cscore, m.uscore AS uscore,
					   pt.value AS publisher, dt.value AS developer,
					   tt.value AS alt_titles,
					   ceducational.value AS educational, cgameplay.value AS gameplay, cgenre.value AS genre,
					       cperspective.value AS perspective, csetting.value AS setting, cvehicular.value AS vehicular
					   {1}
       			FROM {0}                               AS m
				LEFT OUTER JOIN company                AS pt ON pt.ROWID = m.publisher_id
       			LEFT OUTER JOIN company                AS dt ON dt.ROWID = m.developer_id
       			LEFT OUTER JOIN (SELECT title_id, group_concat(value, "", "") AS value
                                 FROM {0}_alt_titles
                                 GROUP BY title_id)    AS tt ON tt.title_id = m.ROWID

				LEFT OUTER JOIN (SELECT teducational.title_id, group_concat(educational.value, "", "") AS value
                                 FROM {0}_educational AS teducational
                                 JOIN educational ON educational.ROWID = teducational.value_id
                                 GROUP BY title_id)    AS ceducational ON ceducational.title_id = m.ROWID
				LEFT OUTER JOIN (SELECT tgameplay.title_id, group_concat(gameplay.value, "", "") AS value
                                 FROM {0}_gameplay AS tgameplay
                                 JOIN gameplay ON gameplay.ROWID = tgameplay.value_id
                                 GROUP BY title_id)    AS cgameplay ON cgameplay.title_id = m.ROWID
				LEFT OUTER JOIN (SELECT tgenre.title_id, group_concat(genre.value, "", "") AS value
                                 FROM {0}_genre AS tgenre
                                 JOIN genre ON genre.ROWID = tgenre.value_id
                                 GROUP BY title_id)    AS cgenre ON cgenre.title_id = m.ROWID
				LEFT OUTER JOIN (SELECT tperspective.title_id, group_concat(perspective.value, "", "") AS value
                                 FROM {0}_perspective AS tperspective
                                 JOIN perspective ON perspective.ROWID = tperspective.value_id
                                 GROUP BY title_id)    AS cperspective ON cperspective.title_id = m.ROWID
				LEFT OUTER JOIN (SELECT tsetting.title_id, group_concat(setting.value, "", "") AS value
                                 FROM {0}_setting AS tsetting
                                 JOIN setting ON setting.ROWID = tsetting.value_id
                                 GROUP BY title_id)    AS csetting ON csetting.title_id = m.ROWID
				LEFT OUTER JOIN (SELECT tvehicular.title_id, group_concat(vehicular.value, "", "") AS value
                                 FROM {0}_vehicular AS tvehicular
                                 JOIN vehicular ON vehicular.ROWID = tvehicular.value_id
                                 GROUP BY title_id)    AS cvehicular ON cvehicular.title_id = m.ROWID
				",
				new object[]{tablen,
					extraColumn.Length > 0 ? ", " + extraColumn + " AS __quality": ""
				}
			);
		}

		private static string Build_no_ref (string tablen, string column, string key, int maxResults)
		{
			return Build_single_ref_direct (tablen, column, key, maxResults);
		}

		private static string Build_single_ref_direct (string tablen, string column, string key, int maxResults)
		{
			return
				_build_base(tablen, string.Format("FUZZY({0}, \"{1}\")", aliases.ContainsKey (column) ? aliases[column] : column, key))
				+ string.Format(@"
				WHERE __quality > 0.0
				ORDER BY __quality DESC, title ASC
                LIMIT {0:D}", maxResults);
		}

		private static string Build_single_ref_indirect (string tablen, string column, string key, int maxResults)
		{
			return _build_base(tablen, string.Format("FUZZY(title, \"{0}\")", key))
				+ @"
				UNION
				"
				+ _build_base(tablen, "tfilter.__maxrefquality")
				+ string.Format(@"
				INNER JOIN (SELECT searchby.title_id AS title_id, group_concat(searchby.value, "", "") AS value, MAX(FUZZY(searchby.value, ""{1}"")) AS __maxrefquality
                            FROM {0}_alt_titles AS searchby
                            WHERE FUZZY(searchby.value, ""{1}"") > 0.0
                            GROUP BY title_id) AS tfilter ON tfilter.title_id = m.ROWID",
				new object[]{tablen, key})
				+ string.Format(@"
				WHERE __quality > 0.0
				ORDER BY __quality DESC, title ASC
                LIMIT {0:D}", maxResults)
			;
		}

		private static string Build_dual_ref_indirect (string tablen, string column, string key, int maxResults)
		{
			return _build_base(tablen, "tfilter.__maxrefquality")
				+ string.Format(@"
				INNER JOIN (SELECT searchby.title_id AS title_id, MAX(FUZZY({1}.value, ""{2}"")) AS __maxrefquality
                            FROM {0}_{1} AS searchby
                            JOIN {1} ON {1}.ROWID = searchby.value_id
                            WHERE FUZZY({1}.value, ""{2}"") > 0.0
                            GROUP BY title_id) AS tfilter ON tfilter.title_id = m.ROWID",
				new object[]{tablen, column, key})
				+ string.Format(@"
					WHERE __quality > 0.0
					ORDER BY __quality DESC, title ASC
			        LIMIT {0:D}", maxResults);
		}
	}
}
